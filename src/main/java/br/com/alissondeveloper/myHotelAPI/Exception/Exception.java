package br.com.alissondeveloper.myHotelAPI.Exception;

public class Exception extends Throwable{

    public Exception(){
        super();
    }
    public Exception(String message){
        super(message);
    }
}
