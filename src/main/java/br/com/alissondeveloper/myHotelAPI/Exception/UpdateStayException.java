package br.com.alissondeveloper.myHotelAPI.Exception;

public class UpdateStayException extends  Exception{
    public UpdateStayException(String message){
        super(message);
    }
}
