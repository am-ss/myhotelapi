package br.com.alissondeveloper.myHotelAPI.controller;


import java.util.List;

import br.com.alissondeveloper.myHotelAPI.modelVOs.ClientVO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import br.com.alissondeveloper.myHotelAPI.models.Client;
import br.com.alissondeveloper.myHotelAPI.services.ClientServices;

@RestController
@Api(value="Clientes")
@RequestMapping("/clients")
public class ClientsController {
	
	@Autowired
	private ClientServices service;
	
	@GetMapping
	public @ResponseBody List<Client> findAll(){
		
		return service.findAll();
	}


	@GetMapping("/{id}")
	public Client findOneClient(@PathVariable("id") Long id){
		return service.getOneClient(id);
	}
	
	@PostMapping
	public Client register(@RequestBody ClientVO client) {
		return service.registerClient(client);
	}
	
}