package br.com.alissondeveloper.myHotelAPI.controller;

import br.com.alissondeveloper.myHotelAPI.modelVOs.RoomVO;
import br.com.alissondeveloper.myHotelAPI.models.Room;
import br.com.alissondeveloper.myHotelAPI.services.RoomServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="Quartos")
@RequestMapping("/rooms")
public class RoomController {

    @Autowired
    RoomServices roomServices;

    @ApiOperation(value = "Retorna todos os quartos")
    @GetMapping
    public @ResponseBody List<Room> findAllRoom(){
        return  roomServices.findAll();
    }

    @ApiOperation(value = "Retorna quarto por id")
    @GetMapping("/{id}")
    public @ResponseBody Room findOneRoom (@PathVariable("id") Long id){
        return roomServices.findById(id);
    }

    @ApiOperation(value = "Cadastra novo quarto")
    @PostMapping
    public RoomVO registerRoom(@RequestBody RoomVO room){
        room.setId(null);
        Room responseRoom = roomServices.registerRoom(room);
        RoomVO responseRoomVO = new RoomVO();
        BeanUtils.copyProperties(responseRoom, responseRoomVO);
        responseRoomVO.setFloorId(responseRoom.getFloor().getId());
        return responseRoomVO;
    }
}
