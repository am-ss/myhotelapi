package br.com.alissondeveloper.myHotelAPI.controller;

import br.com.alissondeveloper.myHotelAPI.models.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value="Home Page")
@RequestMapping("/")
public class myHomePageController {
	
	@GetMapping
	@ApiOperation(value = "Retorna insformações da api")
	public Response showInfoAPI() {

		return new Response(1, "Plataforma: MyHotel");
	}
}
