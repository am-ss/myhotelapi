package br.com.alissondeveloper.myHotelAPI.controller;


import br.com.alissondeveloper.myHotelAPI.modelVOs.FloorVO;
import br.com.alissondeveloper.myHotelAPI.models.Floor;
import br.com.alissondeveloper.myHotelAPI.services.FloorServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="Andar")
@RequestMapping("/floor")
public class FloorController {

    @Autowired
    FloorServices service;

    @ApiOperation(value = "Retorna todos os andares")
    @GetMapping
    public List<Floor> findAll(){
        return service.findAll();
    }

    @ApiOperation(value = "Cadastra um novo andar")
    @PostMapping
    public Floor register(@RequestBody FloorVO floor){
        return service.registerFloor(floor);
    }
}
