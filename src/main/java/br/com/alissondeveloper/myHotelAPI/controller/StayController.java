package br.com.alissondeveloper.myHotelAPI.controller;

import br.com.alissondeveloper.myHotelAPI.Exception.UpdateStayException;
import br.com.alissondeveloper.myHotelAPI.modelVOs.StayVO;
import br.com.alissondeveloper.myHotelAPI.models.Response;
import br.com.alissondeveloper.myHotelAPI.models.Stay;
import br.com.alissondeveloper.myHotelAPI.services.StayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="Estadia")
@RequestMapping("/stay")
public class StayController {
    @Autowired
    private StayService service;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @ApiOperation(value = "Retorna todas as estadias")
    @GetMapping
    public List<Stay> getAllStay(){
        return service.getAllStay();
    }
    @ApiOperation(value = "Retorna estadia por id")
    @GetMapping("/{id}")
    public Stay getOneStay(@PathVariable("id") Long id){
        return service.getOneStay(id);
    }

    @ApiOperation(value = "Cadastra nova estadia")
    @PostMapping
    public Stay registerNewStay(@RequestBody StayVO stayVO){
        return service.RegisterStay(stayVO);
    }

    @ApiOperation(value = "Atualiza status de uma estadia")
    @GetMapping("/updateStatus/{id}/{status_id}")
    public ResponseEntity<?> updateStayStatus(@PathVariable("id") Long id, @PathVariable("status_id") Long statusId) throws UpdateStayException {
        try{
            int result = service.updateStayStatus(id, statusId);
            if(result == 1){
                return ResponseEntity.ok(new Response(200, "atualização realizada com sucesso!"));
            }else{
                return new ResponseEntity<>(new Response(400, "Não foi possivel realizar a consulta"), HttpStatus.BAD_REQUEST);
            }
        }catch (Exception ex){
            logger.info("Erro ao atualizar");
            //return new ResponseEntity<>(new Response(400, "Não foi possivel realizar a consulta"), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(new UpdateStayException("Não foi possivel realizar a consulta"), HttpStatus.BAD_REQUEST);
        }

    }
}
