package br.com.alissondeveloper.myHotelAPI.controller;

import br.com.alissondeveloper.myHotelAPI.modelVOs.EmployeeVO;
import br.com.alissondeveloper.myHotelAPI.modelVOs.EmployeeVORegister;
import br.com.alissondeveloper.myHotelAPI.models.Employee;
import br.com.alissondeveloper.myHotelAPI.models.Room;
import br.com.alissondeveloper.myHotelAPI.services.EmployeeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(value="Funcionários")
@RequestMapping("/employee")
public class EmployeeControler {

    @Autowired
    private EmployeeService service;

    @GetMapping
    public List<EmployeeVO> getAllEmployees(){
        return service.findAll();
    }


    @GetMapping("/{id}")
    public @ResponseBody EmployeeVO getOneEmployee (@PathVariable("id") Long id){
        return service.findOneEmployee(id);
    }

    @PostMapping
    public Employee registerEmployee(@RequestBody EmployeeVORegister employeeVO){
        return service.registerEmployee(employeeVO);
    }


}
