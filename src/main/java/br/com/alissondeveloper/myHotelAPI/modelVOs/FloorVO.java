package br.com.alissondeveloper.myHotelAPI.modelVOs;

import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;


@Data
public class FloorVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "name", nullable=false, length = 30)
    private String name;

    @Column(name = "status", nullable=false, length = 30)
    private Long status;


}
