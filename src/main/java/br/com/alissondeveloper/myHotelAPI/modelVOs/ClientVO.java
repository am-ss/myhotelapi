package br.com.alissondeveloper.myHotelAPI.modelVOs;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
public class ClientVO  implements Serializable {

    private static final long serialVersionUID = 1L;


    @Column(name="username", length = 80, nullable = false)
    private String username;

    @Column(name="cpf", length = 14, unique = true, nullable = false)
    private String cpf;

    @Column(length = 80, unique = true, nullable = false)
    private String email;

    @Column(name="birth_date", nullable = false)
    private Date birthDate;

    @Column(name="is_active", nullable = false)
    private Long isActive;

    @Column(name="register_by", nullable = false)
    private Long registerBy;
}
