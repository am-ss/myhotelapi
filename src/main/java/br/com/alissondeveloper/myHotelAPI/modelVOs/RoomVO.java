package br.com.alissondeveloper.myHotelAPI.modelVOs;

import br.com.alissondeveloper.myHotelAPI.models.Floor;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
public class RoomVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private Long status;
    private Long floorId;
}
