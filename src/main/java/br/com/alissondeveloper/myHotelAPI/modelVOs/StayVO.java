package br.com.alissondeveloper.myHotelAPI.modelVOs;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class StayVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String startDate;
    private String endDate;
    private String comment;
    private String stayValue;
    private Long client;
    private Long stayStatus;
    private Long room;


}
