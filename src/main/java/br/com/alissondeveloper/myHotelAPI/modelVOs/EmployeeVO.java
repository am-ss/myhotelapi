package br.com.alissondeveloper.myHotelAPI.modelVOs;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Data
public class EmployeeVO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private String email;

    private String cpf;

    private Date birthDate;

    private Date createDate;

    private Boolean active;

    private Long registerBy;

}
