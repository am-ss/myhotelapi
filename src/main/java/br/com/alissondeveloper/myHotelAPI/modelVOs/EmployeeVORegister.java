package br.com.alissondeveloper.myHotelAPI.modelVOs;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class EmployeeVORegister implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private String email;

    private String password;

    private String cpf;

    private Date birthDate;

    private Boolean active;

    private Long registerBy;
}
