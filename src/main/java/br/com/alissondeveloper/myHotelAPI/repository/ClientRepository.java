package br.com.alissondeveloper.myHotelAPI.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.alissondeveloper.myHotelAPI.models.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
	
}
