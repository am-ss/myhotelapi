package br.com.alissondeveloper.myHotelAPI.repository;


import br.com.alissondeveloper.myHotelAPI.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query(value = "select * from employee WHERE email = :email", nativeQuery = true)
    public Employee getUserByEmail(@Param("email") String email) throws Exception;
}
