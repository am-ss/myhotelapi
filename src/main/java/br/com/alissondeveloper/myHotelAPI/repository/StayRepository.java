package br.com.alissondeveloper.myHotelAPI.repository;

import br.com.alissondeveloper.myHotelAPI.models.Stay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface StayRepository  extends JpaRepository <Stay, Long> {

    /*@Modifying
    @Query(value = "Update stay t SET t.status_id=:status_id WHERE t.id=:id")
    void changeStayStatus(@Param("id") Long id, @Param("status_id") Long statusId);*/

    @Transactional
    @Modifying
    @Query(value = "update stay SET status_id = :status_id WHERE id = :id", nativeQuery = true)
    public int changeStayStatus(@Param("id") Long id, @Param("status_id") Long status_id) throws Exception;


}
//"UPDATE stay SET status_id = :statusId where id =: stayId"