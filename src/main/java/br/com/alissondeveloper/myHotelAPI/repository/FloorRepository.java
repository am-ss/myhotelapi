package br.com.alissondeveloper.myHotelAPI.repository;

import br.com.alissondeveloper.myHotelAPI.models.Floor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FloorRepository extends JpaRepository<Floor, Long> {
}
