package br.com.alissondeveloper.myHotelAPI.repository;

import br.com.alissondeveloper.myHotelAPI.models.StayStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StayStatusRepository extends JpaRepository<StayStatus, Long> {
}
