package br.com.alissondeveloper.myHotelAPI.repository;

import br.com.alissondeveloper.myHotelAPI.models.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

}
