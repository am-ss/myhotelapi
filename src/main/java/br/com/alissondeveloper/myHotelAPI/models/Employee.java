package br.com.alissondeveloper.myHotelAPI.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table(name="employee")
public class Employee implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="name", nullable = false, length = 80)
	private String name;
	
	@Column(name="email", nullable = false, length = 80)
	private String email;
	
	@Column(name="password", nullable = false, length = 80)
	private String password;
	
	@Column(name="cpf", nullable = false, length = 14, unique = true)
	private String cpf;
	
	@Column(name="birth_date", nullable = false, length = 60)
	private Date birthDate;

	@Column(name="username", nullable = false, length = 80)
	private String username;

	@Column(name="create_date", nullable = false, length = 60)
	private Date createDate;
	
	@Column(name="active", nullable = false)
	private Boolean active;
	
	@Column(name="register_by", nullable = false)
	private Long registerBy;
}
