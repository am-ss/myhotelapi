package br.com.alissondeveloper.myHotelAPI.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table(name = "floor")
public class Floor implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "name", nullable=false, length = 30)
	private String name;
	
	@Column(name = "status", nullable=false, length = 30)
	private Long status;
	
	@Column(name = "create_date", nullable=false, length = 30)
	private Date createDate;
	
	
}
