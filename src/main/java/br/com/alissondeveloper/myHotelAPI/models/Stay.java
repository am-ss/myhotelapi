package br.com.alissondeveloper.myHotelAPI.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="stay")
public class Stay implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="start_date", nullable= false, length = 60)
	private String startDate;
	
	@Column(name="end_date", nullable=false, length = 60)
	private String endDate;
	
	@Column(name="comment", nullable=false, length = 300)
	private String comment;
	
	@Column(name="create_date", nullable = false)
	private Date createDate;
	
	@Column(name="stay_value", nullable = false, length = 20)
	private String stayValue;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="client_id")
	private Client client;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="status_id")
	private StayStatus stayStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="room_id")
	private Room room;
}
