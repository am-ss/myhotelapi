package br.com.alissondeveloper.myHotelAPI.models;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table(name="stay_status")
public class StayStatus implements Serializable{
		
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name="description", nullable = false, length = 20)
	private String description;
	
	

}
