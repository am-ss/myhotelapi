package br.com.alissondeveloper.myHotelAPI.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
@Table(name = "room")
public class Room implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name", nullable = false, length = 50)
	private String name;
	
	@Column(name = "status", nullable = false, length = 50)
	private Long status;
	
	@Column(name = "create_date", nullable = false, length = 50)
	private Date createDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="floor_id")
	private Floor floor;

	/*@Column(name = "floor_id", nullable = false, length = 50)
	private Long floorId;*/



	
}
