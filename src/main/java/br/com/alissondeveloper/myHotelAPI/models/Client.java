package br.com.alissondeveloper.myHotelAPI.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name="clients")
public class Client implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="user_name", length = 80)
	private String userName;
	
	@Column(name="cpf", length = 14, unique = true, nullable = false)
	private String cpf;

	@Column(length = 80, unique = true, nullable = false)
	private String email;
	
	@Column(name="birth_date", nullable = false)
	private Date birthDate;
	
	@Column(name="register_date", nullable = false)
	private Date registerDate;
	
	@Column(name="is_active", nullable = false)
	private Long isActive;
	
	@Column(name="register_by", nullable = false)
	private Long registerBy;

}
