package br.com.alissondeveloper.myHotelAPI.services;

import br.com.alissondeveloper.myHotelAPI.modelVOs.FloorVO;
import br.com.alissondeveloper.myHotelAPI.models.Floor;
import br.com.alissondeveloper.myHotelAPI.models.Room;
import br.com.alissondeveloper.myHotelAPI.repository.FloorRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class FloorServices {

    @Autowired
    FloorRepository repository;

    public List<Floor> findAll(){

        return repository.findAll();
    }

    public Floor findById(Long id){
        return repository.getOne(id);
    }

    public Floor registerFloor(FloorVO floorVo){
        Floor floor = new Floor();
        BeanUtils.copyProperties(floorVo, floor);
        floor.setCreateDate(nowDate());
        return repository.save(floor);
    }



    public Date nowDate(){

        Date date = new Date(System.currentTimeMillis());
        return date;

    }
}
