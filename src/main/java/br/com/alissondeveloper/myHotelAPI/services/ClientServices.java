package br.com.alissondeveloper.myHotelAPI.services;

import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import br.com.alissondeveloper.myHotelAPI.modelVOs.ClientVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alissondeveloper.myHotelAPI.models.Client;
import br.com.alissondeveloper.myHotelAPI.repository.ClientRepository;

@Service
public class ClientServices {
	
	@Autowired
	ClientRepository repository;
	
	public List<Client> findAll(){
		
		return repository.findAll();
	}

	public Client getOneClient(Long id){
		return repository.getOne(id);
	}
	
	public Client registerClient(ClientVO clientVo) {
		Client client = new Client();
		BeanUtils.copyProperties(clientVo,client);
		client.setRegisterDate(nowDate());
		return repository.save(client);
	}




	public Date nowDate(){

		Date date = new Date(System.currentTimeMillis());
		return date;

	}


}
