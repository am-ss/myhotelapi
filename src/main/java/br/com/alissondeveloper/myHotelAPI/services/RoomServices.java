package br.com.alissondeveloper.myHotelAPI.services;

import br.com.alissondeveloper.myHotelAPI.modelVOs.RoomVO;
import br.com.alissondeveloper.myHotelAPI.models.Room;
import br.com.alissondeveloper.myHotelAPI.repository.FloorRepository;
import br.com.alissondeveloper.myHotelAPI.repository.RoomRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class RoomServices {

    @Autowired
    RoomRepository roomRepository;

    @Autowired
    FloorServices floorServices;

    public List<Room> findAll(){
        return roomRepository.findAll();
    }

    public Room findById(Long id){
        return roomRepository.getOne(id);
    }

    public Room registerRoom(RoomVO roomVo) {
        Room room = new Room();
        BeanUtils.copyProperties(roomVo, room);
        room.setCreateDate(nowDate());
        room.setFloor(floorServices.findById(roomVo.getFloorId()));
        return roomRepository.save(room);
    }

    public Date nowDate(){
        Date date = new Date(System.currentTimeMillis());
        return date;
    }

}
