package br.com.alissondeveloper.myHotelAPI.services;


import br.com.alissondeveloper.myHotelAPI.modelVOs.EmployeeVO;
import br.com.alissondeveloper.myHotelAPI.modelVOs.EmployeeVORegister;
import br.com.alissondeveloper.myHotelAPI.models.Employee;
import br.com.alissondeveloper.myHotelAPI.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class EmployeeService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private EmployeeRepository repository;

    public List<EmployeeVO> findAll(){
        List<Employee> employeeList = repository.findAll();
        List<EmployeeVO> employeeVOList = new ArrayList<EmployeeVO>();
        for (Employee emp: employeeList) {
            EmployeeVO employeeVO = new EmployeeVO();
            BeanUtils.copyProperties(emp, employeeVO);
            employeeVOList.add(employeeVO);
        }
        return employeeVOList;
    }

    public EmployeeVO findOneEmployee(Long id){
        Employee employee = repository.getOne(id);
        EmployeeVO employeeVO = new EmployeeVO();

        BeanUtils.copyProperties(employee, employeeVO);

        return employeeVO;
    }

    public Employee registerEmployee(EmployeeVORegister employeeRegister){
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeRegister, employee);
        employee.setCreateDate(nowDate());
        return repository.save(employee);

    }

    public Employee getByEmail(String email){
        try{
            return repository.getUserByEmail(email);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
            return null;
        }

    }

    public Date nowDate(){
        Date date = new Date(System.currentTimeMillis());
        return date;
    }





}
