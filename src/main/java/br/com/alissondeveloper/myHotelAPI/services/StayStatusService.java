package br.com.alissondeveloper.myHotelAPI.services;

import br.com.alissondeveloper.myHotelAPI.models.StayStatus;
import br.com.alissondeveloper.myHotelAPI.repository.StayRepository;
import br.com.alissondeveloper.myHotelAPI.repository.StayStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StayStatusService {

    @Autowired
    StayStatusRepository repository;

    public StayStatus getOneStayStatus (Long id){
        return repository.getOne(id);
    }

}
