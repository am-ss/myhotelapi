package br.com.alissondeveloper.myHotelAPI.services;

import br.com.alissondeveloper.myHotelAPI.Exception.UpdateStayException;
import br.com.alissondeveloper.myHotelAPI.modelVOs.StayVO;
import br.com.alissondeveloper.myHotelAPI.models.Client;
import br.com.alissondeveloper.myHotelAPI.models.Stay;
import br.com.alissondeveloper.myHotelAPI.repository.StayRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StayService {

    @Autowired
    StayRepository repository;

    @Autowired
    StayStatusService stayStatusService;

    @Autowired
    ClientServices clientServices;

    @Autowired
    RoomServices roomServices;

    private final Logger logger = LoggerFactory.getLogger(getClass());


    public List<Stay> getAllStay(){
        return repository.findAll();
    }

    public Stay getOneStay (Long id){
        return repository.getOne(id);
    }

    public Stay RegisterStay (StayVO stayVO){
        Stay stay = new Stay();
        BeanUtils.copyProperties(stayVO, stay);
        stay.setCreateDate(nowDate());
        stay.setClient(clientServices.getOneClient(stayVO.getClient()));
        stay.setStayStatus(stayStatusService.getOneStayStatus(stayVO.getStayStatus()));
        stay.setRoom(roomServices.findById(stayVO.getRoom()));
        return repository.save(stay);
    }
    public int updateStayStatus (Long id, Long StatusId) throws Exception{
        try{
            int result = repository.changeStayStatus(id, StatusId);
            logger.info(result + "");
            return result;
        }catch (Exception ex){
            logger.error(ex.getMessage());
            return  0;
        }

    }

   public Date nowDate(){
        Date date = new Date(System.currentTimeMillis());
        return date;
    }


}
