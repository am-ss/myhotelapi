package br.com.alissondeveloper.myHotelAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class MyHotelApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyHotelApiApplication.class, args);
	}

}
