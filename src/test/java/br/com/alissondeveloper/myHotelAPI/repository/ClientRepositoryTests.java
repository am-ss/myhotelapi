package br.com.alissondeveloper.myHotelAPI.repository;


import br.com.alissondeveloper.myHotelAPI.models.Client;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ClientRepositoryTests {

    @Autowired
    private ClientRepository clientRepository;

    @Test
    public void allClientsTest(){
        List<Client> users = clientRepository.findAll();
        assertThat(users.size()).isGreaterThan(0);
    }
}
